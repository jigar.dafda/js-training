# JavaScript Training

*Note: This Traning is only about JavaScript Concepts. its not specific to any environment.*
*All these concepts should apply all browsers and NodeJS*

## Beginner: *estimated 2-3 days required*

1. Basic Data Types - primitive: [`String`, `Number`, `Boolean`, `Object`, `Undefined`], non-primitive: [`Object`, `Array`, `RegExp`]
    - http://www.w3schools.com/js/js_datatypes.asp
2. Control Flows - if condition, ternary operator, while loops, for loops, switch
3. Operators - Arithmetic, Assignment, String, Comparison, Conditional, Logical, Bitwise
    - http://www.w3schools.com/jsref/jsref_operators.asp
4. What is `NaN` ?
5. Falsy values  - `false`, `0`, `""`, `null`, `undefined`, 
6. Use of `typeof`, `instanceof`
7. Use of `var`
8. Use Logical Operators instead of if conditions.
9. Explore Nativa Methods of `Array` - `forEach`, `find`, `map`, `sort`, `length`, `push`, `shift`, `indexOf`, `concat`, `join` etc.
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
10. Explore `String` Methods - `split`, `splice` etc.
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
11. Explore `Date` Methods
    - https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Date
11. Explore `Math` Methods
    - https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Math
12. Timers - `setTimeout`, `setInterval`, `clearTimeout`
13. Explore Ways of creating objects
    - http://a-developer-life.blogspot.in/2011/11/7-ways-to-create-objects-in-javascript.html
    
---
        
## Intermediate: *estimated 4-5 days required*
1. Scope and context
    - http://www.w3schools.com/js/js_scope.asp
    - http://ryanmorr.com/understanding-scope-and-context-in-javascript/
    - http://stackoverflow.com/a/1484230
2. closures
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures
3. Immediately - invoked function expression and its benefits
4. OOJS (Object Orieinted JavaScript)
    - Classes using constructor functions
    - What is `this` ?
    - Prototype - __ proto __, Fn.prototype
5. `'use strict'` - Strict Mode;
6. Variable Hoisting
7. Function Hoisting

---

## Advanced: *estimated 4-5 days required*
1. ES6 Basics 
    - `let` , `const`, `enum`
    - ES6 `class`, constuctors
    - Promises
    - Template String
    - `import` and `export`
    - diff b/w arrow Function and Normal Function
        - Enhanced Object Literals
    - ES6 to ES5 - Conversions manually 

---

## Expert: *estimated 4-5 days required*
1. ES6
    - generators
2. ES7
    - Decorator
